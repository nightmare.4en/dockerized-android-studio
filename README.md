# Dockerized Android Studio
Um Android Studio que é executado dentro de um container do Docker.

---
### Prerequistos:
- [Docker](https://www.docker.com/)

---
### Instalação:
Para instalar o Android Studio execute o script `install.sh`.
##### Observações:
- O script irá baixar o Android Studio (2.3.3), montar a imagem do docker e baixar/installar os requisitos necessário para exectutar o Android Studio dentro de um container.
- Os arquivos necessários para seu funcionamento serão mantidos na pasta *volume* que será gerada no local de execução do script.

---
### Execução:
Para abrir o Android Studio execute o script `start.sh`.
##### Observações:
- O script irá subir um container da imagem previamente gerada e abrir o Android Studio.
- Para mudar o caminho do Android SDK que ele irá usar, você pode digitá-lo na frente ao executar o script, da seguinte forma: `./start.sh caminho/parada/Android/Sdk`
- Para abrir um projeto no Android Studio, mova ele para a pasta *AndroidStudioProjects* que será gerada no local de execução do script. Dessa forma ele ficará disponível na pasta */home/docker-nome-do-seu-uario/AndroidStudioProjects* dentro do container.
