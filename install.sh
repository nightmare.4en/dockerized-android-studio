#!/bin/bash

FILE="android-studio-ide-162.4069837-linux.zip"
URL="https://dl.google.com/dl/android/studio/ide-zips/2.3.3.0/${FILE}"

mkdir -p $(pwd)/AndroidStudioProjects
mkdir -p $(pwd)/volume/AndroidStudio2.3
mkdir -p $(pwd)/volume/gradle
mkdir -p $(pwd)/volume/android
mkdir -p $(pwd)/volume/crashlytics
mkdir -p $(pwd)/volume/java
sudo chown ${UID}:${UID} -R $(pwd)/volume

if [ ! -f $(pwd)/${FILE} ]
then
  curl "${URL}" > $(pwd)/${FILE}
fi
unzip -q $(pwd)/${FILE} -d $(pwd)/volume

docker build \
--build-arg FILE=${FILE} \
--build-arg USER=${USER} \
--build-arg UID=${UID} \
--no-cache=false \
-t android-studio .
