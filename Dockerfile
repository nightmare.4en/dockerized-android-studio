FROM java

ARG UID
ARG USER
ARG FILE

# Instalando dependecias
RUN dpkg --add-architecture i386
RUN apt-get -qq update
RUN apt-get -qq install -y sudo
RUN apt-get -qq install -y zip
RUN apt-get -qq install -y lib32z1
RUN apt-get -qq install -y lib32ncurses5
RUN apt-get -qq install -y libbz2-1.0:i386
RUN apt-get -qq install -y lib32stdc++6
RUN apt-get -qq clean

# Instalando Android Studio
ADD /volume/android-studio /opt/android-studio
RUN chown ${UID}:${UID} -R /opt/android-studio

# Configura X
RUN export uid=${UID} gid=${UID} && \
    mkdir -p /home/docker-${USER} && \
    echo "docker-${USER}:x:${uid}:${gid}:docker-${USER},,,:/home/docker-${USER}:/bin/bash" >> /etc/passwd && \
    echo "docker-${USER}:x:${uid}:" >> /etc/group && \
    echo "docker-${USER} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/docker-${USER} && \
    chmod 0440 /etc/sudoers.d/docker-${USER} && chown ${uid}:${gid} -R /home/docker-${USER}
USER docker-${USER}
ENV HOME /home/docker-${USER}

CMD /opt/android-studio/bin/studio.sh
