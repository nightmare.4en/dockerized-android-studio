SDKPATH="${HOME}/Android/Sdk"

if [ $1 ]
then
  if [ -d $1 ]
  then
    echo "Caminho para Adroid SDK será $1."
  else
    echo "Erro: Esse caminho não existe."
    exit
  fi
else
  echo "Caminho do Android SDK não informando. Usando caminho padrão ${SDKPATH}"
fi

docker run -ti -e DISPLAY=unix$DISPLAY --privileged -v /dev/bus/usb:/dev/bus/usb \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v $(pwd)/AndroidStudioProjects:/home/docker-${USER}/AndroidStudioProjects \
-v ${SDKPATH}:/home/docker-${USER}/Android/Sdk \
-v $(pwd)/volume/AndroidStudio2.3:/home/docker-${USER}/.AndroidStudio2.3 \
-v $(pwd)/volume/gradle:/home/docker-${USER}/.gradle \
-v $(pwd)/volume/android:/home/docker-${USER}/.android \
-v $(pwd)/volume/crashlytics:/home/docker-${USER}/.crashlytics \
-v $(pwd)/volume/java:/home/docker-${USER}/.java \
android-studio
